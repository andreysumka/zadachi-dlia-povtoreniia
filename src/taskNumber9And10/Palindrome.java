package taskNumber9And10;

/**
 * Программа, проверяющая является ли число или строка палиндромом
 *
 * @author Sumka Andrey 18it18
 */

public class Palindrome {
    public static void main(String[] args) {
        System.out.println(isPalindrome(Integer.toString(3332)));
        System.out.println(isPalindrome(Integer.toString(9449)));
        System.out.println(isPalindrome("Кабак"));
        System.out.println(isPalindrome(" Нажал кабан на баклажан "));
    }
    private static boolean isPalindrome(String src) {
        src = src.replaceAll("[\\s]", "").toLowerCase();
        boolean result = true;
        for (int i = 0; i < src.length() / 2; i++) {
            if (src.charAt(i) != src.charAt(src.length() - i - 1)) {
                result = false;
                break;
            }
        }
        return result;
    }
}
