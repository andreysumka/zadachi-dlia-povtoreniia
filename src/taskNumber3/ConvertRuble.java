package taskNumber3;

import java.util.Scanner;

/**
 * Программа для перевода из рубля в евро и вывод курса в рублях
 *
 * @author Sumka Andrey 18it18
 */

public class ConvertRuble {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Курс рубля: ");
        double Course = scanner.nextDouble();
        System.out.print("Сколько рублей хотите перевести: ");
        double Count = scanner.nextDouble();
        System.out.print("В евро: ");
        System.out.printf("%.2f",convert(Course, Count));
    }

    private static double convert(double Course, double Сount) {
        return Сount / Course;
    }
}
