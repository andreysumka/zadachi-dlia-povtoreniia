package taskNumber2;

/**
 * Программа для увеличения элемента массива на 10%
 *
 * @author Sumka Andrey 18it18
 */


public class ArrayIncrease {
    public static void main(String[] args) {
        double[] num = new double[5];

        num[0] = 20;
        num[1] = 55;
        num[2] = 10;
        num[3] = 100;
        num[4] = 7.02;

        for (int i = 0; i < num.length; i++) {
            num[i] = (num[i] * 1.1);
            System.out.println(num[i]);
        }
    }
}

