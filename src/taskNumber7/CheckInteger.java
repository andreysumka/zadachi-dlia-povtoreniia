package taskNumber7;

import java.util.Scanner;

/**
 * Программа, проверяющая является ли число типа double целым
 *
 * @author Sumka Andrey 18it18
 */

public class CheckInteger {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число для проверки: ");
        double x = sc.nextDouble();
        if (x % 1 == 0) {
            System.out.println("Число целое");
        } else {
            System.out.println("Это не целое число");
        }
    }
}
