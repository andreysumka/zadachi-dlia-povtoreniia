package taskNumber14;

/**
 * Программа, меняющая в матрице столбцы и строки местами
 *
 * @author Sumka Andrey 18it18
 */

public class Matrix {
    public static void main(String[] args) {
        int [] [] array = new int[3][3];

        for (int line = 0; line < array.length; line++) {
                System.out.println();
                for (int column = 0; column < array.length; column++) {
                    array[line][column] = (int)((Math.random() * 9));
                    System.out.println(array[line][column] + " ");
                }
        }
        System.out.println("\n");

        for (int i = 0; i < 3; i++) {
            for (int j = i + 1; j < 3; j++) {
                int reversesArray = array[i][j];
                array[i][j] = array[j][i];
                array[j][i] = reversesArray;
            }
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.println(array[i][j] + "");
            }
            System.out.println();
        }
    }
}
