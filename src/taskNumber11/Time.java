package taskNumber11;

import java.util.Scanner;

/**
 * Программа для нахождения часов, минут и секунд в определенный временной промежуток
 *
 * @author Sumka Andrey 18it18
 */

public class Time {
    public static void main(String[] args) {
        System.out.print("Введите количество дней: ");
        int days = new Scanner(System.in).nextInt();

        convert(days);

    }

    private static void convert(int days) {
        if (days <= 0) {
            System.out.println("Не верно введен временной промежуток!");
        }
        if (days == 1) {
            System.out.println("В " + days + " дне: " +
                    days * 24 + " часов, " +
                    days * 1440 + " минут, " +
                    days * 86400 + " секунд.");
        }
        if (days > 1) {
            System.out.println("В " + days + " днях: " +
                    days * 24 + " часов, " +
                    days * 1440 + " минут, " +
                    days * 86400 + " секунд.");
        }
    }
}
