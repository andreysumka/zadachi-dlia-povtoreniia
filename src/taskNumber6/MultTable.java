package taskNumber6;

import java.util.Scanner;

/**
 * Программа для нахождения таблицы умножения для введенного числа
 *
 * @author Sumka Andrey 18it18
 */

public class MultTable {
    public static void main(String[] args) {
        System.out.print("Введите число:  ");
        int number = new Scanner(System.in).nextInt();

        multTable(number);

    }

    private static void multTable(int number) {
        for (int i = 1; i <= 10; i++) {
            System.out.println(number + " * " + i + " = " + number * i);
        }
    }
}
