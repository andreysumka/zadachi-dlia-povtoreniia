package taskNumber8;

import java.util.Scanner;

/**
 * Программа, которая в качестве аргумента получает число и полностью обнуляет столбец прямоугольной матрицы, который соответствует заданному числу
 *
 * @author Sumka Andrey 18it18
 */

public class ColumnMatrix {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Введите число: ");
        int num = sc.nextInt()-1;
        int [][] matrix = new int [5][6];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.println((matrix[i][j] = 1 + (int)(Math.random() * 10)) + "");
            }
            System.out.println("\n");
        }
        System.out.println("\n");
        nullifiction(num,matrix);
    }

    public static void nullifiction(int num, int [][]matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if(j==num){
                    matrix[i][j] = 0;
                }
                System.out.println(matrix[i][j] + "");
            }
            System.out.println("\n");
            }
    }
}
