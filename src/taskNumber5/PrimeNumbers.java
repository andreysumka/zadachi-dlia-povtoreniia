package taskNumber5;

/**
 * Программа с выводом простых чисел от 2 до 100
 *
 * @author Sumka Andrey 18it18
 */

public class PrimeNumbers {
    public static void main(String[] args) {
// Простые числа делятся без остатка только на 1 и на себя.
// Нужно посчитать делители, которых должно быть не больше 2.
        for (int i = 2; i <= 100; i++) {
            int v = 0;
            for (int j = 2; j < i; j++) {
                if ((i % j) == 0)
                    v++;
            }
            if (v < 2)
                System.out.println(i + " - простое число");
        }
    }
}
