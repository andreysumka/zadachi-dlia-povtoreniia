package taskNumber12;

import java.util.Arrays;

/**
 * Программа, переносящая числа из двумерного массива в одномерный массив построчно
 *
 * @author Sumka Andrey 18it18
 */

public class Massive {
    public static void main(String[] args) {
        int [] [] array = new int[3][3];

        for (int line = 0; line < array.length; line++) {
            System.out.println();
            for (int column = 0; column < array.length; column++) {
                array[line][column] = (int)(-10 + (Math.random() * 20));
                System.out.println(array[line][column] + " ");
            }
        }
        System.out.println();
        System.out.println(Arrays.deepToString(array));
    }
}
