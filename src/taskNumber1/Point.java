package taskNumber1;

import java.util.Scanner;

/**
 * Программа считывающая символы до точки и количество пробелов при вводе текста
 *
 * @author Sumka Andrey 18it18
 */

public class Point {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите строку: ");
        String text = sc.nextLine();
        int point = text.indexOf(".");//кол-во символов до точки
        System.out.println("Символы до точки - " + point);
        String space = text.split("\\.", 2)[0];//кол-во пробелов до точки
        System.out.print("Количество пробелов = ");
        System.out.println(space.length() - space.replaceAll(" ", "").length());//Убираем все пробелы из строки, а потом считаем разницу длин
    }
}
